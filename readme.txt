##HOW TO INSTALL

#NOTICE:: THIS IS A LARAVEL FRAMEWORK

Instructions:

1. Point the virtual server config to this folder "exam/public"
2. Terminal : Type in - "php composer.phar update"
3. Load the "username database" in your database.
4. Configure the database connection in "exam/app/config/database.php".
5. Terminal : Type in - "php artisan migrate"
6. Terminal : Type in - "php artisan db:seed" 

7. Go to the url you have defined in your host file.